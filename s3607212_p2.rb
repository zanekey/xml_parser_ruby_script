require 'nokogiri'
require 'ruby-lint'


#command constants
XML_COMMAND= "-xml"
HELP_COMMAND = "help"
LIST_COMMAND = "list"
IP_COMMAND = "--ip"
NAME_COMMAND = "--name"
SEARCH_IP = '//ip_address'
SEARCH_NAME='//first_name'
SEARCH_LASTNAME ='//last_name'
#open file requested through command line
def fileReader(filename)
    xmlFile = File.open(filename)
    $xmlDoc = Nokogiri::XML(xmlFile)
end
#method to print records in JSON format
def recordPrinter(arrayOfRecords)
    if arrayOfRecords.length == 0
        return
    end
    i = 0
    while i < arrayOfRecords.length
        record = arrayOfRecords[i]
        puts '{ "id": "' + record.xpath('id').text + '"'
        puts '"first_name": "' + record.xpath('first_name').text + '"'
        puts '"last_name": "' + record.xpath('last_name').text + '"'
        puts '"email": "' + record.xpath('email').text + '"'
        puts '"gender": "' + record.xpath('gender').text + '"'
        puts '"ip_address": "' + record.xpath('ip_address').text + '"'
        puts '"send_date": "' + record.xpath('send_date').text + '"'
        puts '"email_body": "' + record.xpath('email_body').text + '"'
        puts '"email_title": "' + record.xpath('email_title').text + '"'
        puts '},'

        i += 1
    end
end
#finds records through Xpath
def pathFinder(searchType , searchTerm)
    recordArray = []

    $xmlDoc.xpath(searchType).each do |search|
        if searchType == SEARCH_IP
            if search.text.eql?searchTerm
                recordArray << search::parent
            end
        else 
            if search.text.downcase.include?searchTerm.downcase
                recordArray << search::parent
            end
        end
    end
    return recordArray
end    
#print executable program commands
def printCommands()
    puts "Commands:"
    puts "3607212_p2.rb	-xml	            [filename]		#Load	a	XML	file"
    puts "s3607212_p2.rb help	                            #Describe	available	commands	or	one	specific	command"
    puts "ruby	s3607212_p2.rb	list --ip	[IP ADDRESS]    #laod record of specified IP-ADDRESS"
    puts "ruby	s3607212_p2.rb	list --name	[NAME]          #load record of specified name (case-sensitive) "
end
#print details
def printDetails()
    puts "Commands:"
    puts "3607212_p2.rb	-xml	[filename]		Load	a	XML	file"
    puts "s3607212_p2.rb	help	[COMMAND]	Describe	available	commands	or	one	specific	command"
end
#open file from directory
def openFileFromDirectory()
    begin
        xmlFile = Dir.glob('*.xml').first
        fileFound = File.open(xmlFile)
        $xmlDoc = Nokogiri::XML(fileFound)
    rescue StandardError
        puts "No xml File Found"
        exit
    end

end 
#input handler 
def queryHandler(secondInput , thirdInput)
    if secondInput == IP_COMMAND
        array = pathFinder(SEARCH_IP ,thirdInput)
        recordPrinter(array)
    elsif secondInput == NAME_COMMAND
        array = pathFinder(SEARCH_NAME ,thirdInput)
        recordPrinter(array)
        array = pathFinder(SEARCH_LASTNAME ,thirdInput)
        recordPrinter(array)
    end
end
#program execution handler
def programExecution()
    firstInput,secondInput ,thirdInput = ARGV[0] , ARGV[1] , ARGV[2]
    if ARGV.empty?
        printDetails()
        return
    end

    case firstInput
        when XML_COMMAND
            fileReader(secondInput)
            queryHandler(ARGV[3],ARGV[4])
        when HELP_COMMAND
            printCommands()
        when LIST_COMMAND
            openFileFromDirectory()
            queryHandler(secondInput,thirdInput)
        else
        puts "input valid commands"
    end
end
#run program
programExecution()