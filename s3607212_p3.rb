require 'nokogiri'
require 'ruby-lint'
require 'date'

#decalring CONSTANTS
XML_COMMAND= "-xml"
HELP_COMMAND = "help"
BEFORE_COMMAND = '-before'
AFTER_COMMAND = '-after'
DAY_COMMAND = '-day'

# Assumptions made 
# Date can only be entred in YYYY-MM format or YYYY
# if using -before and -after commands in conjunction -> -before must always before the -after command
# abbreviations for the days can be as short as two charachters , for example searching for thursday records ->ruby	s123456_p3.rb -day th 

def programExec(commandLineInput)
    input1 , input2 , *splat = ARGV

    if input1 == XML_COMMAND
        fileReader(input2)  
        commandLineInput = *splat
    else 
        openFileFromDirectory()
    end
    
    if commandLineInput.length == 4
        if conditionCheck(commandLineInput)
            #call method to handle request
            before_date = dateStringFormater(commandLineInput[1])
            after_date = dateStringFormater(commandLineInput[3])
            findRecordsByDateRange(before_date,after_date)
        else
            puts "Invalid range between two dates , beforeData must be later than the afterDate "
        end
    elsif commandLineInput.length == 2
            case commandLineInput[0]
            when HELP_COMMAND
                printCommands()
            when BEFORE_COMMAND
                #run before command
                dateSpecified = dateStringFormater(commandLineInput[1])
                findRecordsByDate(dateSpecified,BEFORE_COMMAND)
            when AFTER_COMMAND
                #run after_command
                dateSpecified = dateStringFormater(commandLineInput[1])
                findRecordsByDate(dateSpecified,AFTER_COMMAND)
            when DAY_COMMAND
                #run day command
                daySpecified = commandLineInput[1]
                findRecordsByDay(daySpecified)
            else
                puts "unknown command"
            end
    end
end
#open file requested through command line
def fileReader(filename)
    begin
        xmlFile = File.open(filename)
        $xmlDoc = Nokogiri::XML(xmlFile)
    rescue StandardError
        puts "xml File Not Found"
        exit   
    end
end
#print executable program commands
def printCommands()
    puts "Commands:"
    puts "3607212_p2.rb	-xml	            [filename]		#Load	a	XML	file"
    puts "s3607212_p2.rb help	                            #Describe	available	commands	or	one	specific	command"
    puts "ruby	s123456_p3.rb	-after YYYY-MM/YYYY         #laod record after specified address"
    puts "ruby	s123456_p3.rb	-before YYYY-MM/YYYY        #load before of specified address"
    puts "ruby	s123456_p3.rb	-day wed                    #load records from specified day"
end
# method to format provided substring
def dateStringFormater(dateInput)
    #acquire year substring from date input
    yearSubstring = dateInput[0..3]
    year = /\d{4}/.match(yearSubstring)
    #acquire month substring from date input - if it exists
    monthSubstring = dateInput[5..7]
    month = /\d{2}/.match(monthSubstring)
    # if month exists , check that its a valid month number
    if month && (month.string.to_i < 1 || month.string.to_i > 12)
        puts "Invalid month Input : #{month.string}" 
        exit
    end

    #return what was provided
    if year && month 
        dateFormat = (year.string + month.string)
        return dateFormat
    elsif year
        return year.string
    end

    return NIL
end
# finding records between two dates
def findRecordsByDateRange (beforeDate , afterDate)
    #determine if month and year provided or just year 
    if beforeDate.length == 6
        bfDate = DateTime.strptime(beforeDate,"%Y%m")
    else
        bfDate = DateTime.strptime(beforeDate,"%Y")
    end

    if afterDate.length == 6
        afDate = DateTime.strptime(afterDate,"%Y%m")
    else
        afDate = DateTime.strptime(afterDate,"%Y")
    end

    
    #iterate through xml document and find matching results
    $xmlDoc.xpath('//send_date').each do |date|
    tempDate = DateTime.parse(date.text)
    if (tempDate < bfDate) && (tempDate > afDate)
        record = date::parent
        recordPrinter(record)
    end
    end
end
# finding records by day
def findRecordsByDay (dayString)
    if dayString.length == 1
        puts "Error: Day input not valid , eg -> for {Wednesday  ->, we , wed ,wED , Wednesday ,wednes...} are all valid in any case level"
        exit 
    end

    $xmlDoc.xpath('//send_date').each do |date|
    tempDate = DateTime.parse(date.text)
    formatedString = tempDate.strftime('%A')
    lengthBoolean  = (dayString.length <= formatedString.length) && (dayString.length != 1)

        if (formatedString.downcase.include?(dayString.downcase)) && lengthBoolean
            record = date::parent
            recordPrinter(record)
        end
    end
end
#open file from directory
def openFileFromDirectory()
    begin
        xmlFile = Dir.glob('*.xml').first
        fileFound = File.open(xmlFile)
        $xmlDoc = Nokogiri::XML(fileFound)
    rescue StandardError
        puts "No xml File Found"
        exit
    end

end 
#validation date range is valid  #-> (beforeDate > afterDate)
def validDateRange(beforeDate , afterDate)

    if beforeDate.length == 4
        date1 = DateTime.strptime(beforeDate,"%Y")
    else
        date1 = DateTime.strptime(beforeDate,"%Y%m")
    end
    
    if afterDate.length == 4
        date2 = DateTime.strptime(afterDate,"%Y")
    else
        date2 = DateTime.strptime(afterDate,"%Y%m")   
    end
    
    if date1 > date2
        return true
    else
        return false
    end
end
#validating input is in correct format -> -before [YYYY-MM/YYYY] -after [YYYY-MM/YYYY]
def conditionCheck (commandsString)

    condition1 = (commandsString[0] == BEFORE_COMMAND) && (commandsString[2] == AFTER_COMMAND)    
    beforeDate = dateStringFormater(commandsString[1])
    afterDate = dateStringFormater(commandsString[3])
    conditionValidDateRange = validDateRange(beforeDate , afterDate)

    if condition1 && conditionValidDateRange
        return true
    else
        return false
    end
end
#finding records by before or after commands -> -after [YYYY-MM/YYYY] || -before [YYYY-MM/YYYY] 
def findRecordsByDate (dateInput , condition)
    #determine if month and year provided or just year 
    if dateInput.length == 6
        specicifiedDate = DateTime.strptime(dateInput,"%Y%m")
    else
        specicifiedDate = DateTime.strptime(dateInput,"%Y")
    end
    #iterate through xml document and find matching results
    $xmlDoc.xpath('//send_date').each do |date|
    tempDate = DateTime.parse(date.text)

    if condition == BEFORE_COMMAND
        if tempDate < specicifiedDate
            record = date::parent
            recordPrinter(record)
        end
    elsif condition == AFTER_COMMAND
        if tempDate > specicifiedDate
            record = date::parent
            recordPrinter(record)
        end
    end
    end
end
#print found records in JSON format
def recordPrinter(record)
    puts '{ "id": "' + record.xpath('id').text + '"'
    puts '"first_name": "' + record.xpath('first_name').text + '"'
    puts '"last_name": "' + record.xpath('last_name').text + '"'
    puts '"email": "' + record.xpath('email').text + '"'
    puts '"gender": "' + record.xpath('gender').text + '"'
    puts '"ip_address": "' + record.xpath('ip_address').text + '"'
    puts '"send_date": "' + record.xpath('send_date').text + '"'
    puts '"email_body": "' + record.xpath('email_body').text + '"'
    puts '"email_title": "' + record.xpath('email_title').text + '"'
    puts '},'
end
#command line arguments
commandLineInput = ARGV
#execute program
programExec(commandLineInput)